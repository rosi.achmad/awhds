<?php
$serverName = "172.28.2.216"; 
$connectionInfo = array( "Database"=>"AWHDS-MIRROR", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

if ($_POST["date1"] && $_POST["date2"]){
	$sql = "select substring(convert(varchar(10),DateTime,120),1,10) as tgl, 
			SUM(CASE  WHEN  CallTypeID ='5168' THEN CallsOffered  END) AS Medan,
			SUM(CASE  WHEN  CallTypeID in ('5026','5328','5030','5034','5165') THEN CallsOffered END) AS Jakarta,
			SUM(CASE  WHEN  CallTypeID ='5166' THEN CallsOffered END) AS Surabaya,
			SUM(CASE  WHEN  CallTypeID ='5167' THEN CallsOffered END) AS Makasar,
			SUM(CASE  WHEN  CallTypeID ='5103' THEN CallsOffered END) AS Prompter_Error,
			SUM(CASE  WHEN  CallTypeID in ('5026','5328') THEN CallsOffered END) AS MKios,
			SUM(CASE  WHEN  CallTypeID ='5034' THEN CallsOffered END) AS Long_Number,
			SUM(CASE  WHEN  CallTypeID ='5030' THEN CallsOffered END) AS International
			from t_Call_Type_Interval
			Where DateTime between '".$_POST["date1"]." 00:00:00' and '".$_POST["date2"]." 23:45:00' 
			GROUP BY substring(convert(varchar(10),DateTime,120),1,10) ORDER BY substring(convert(varchar(10),DateTime,120),1,10) ASC";			

	$stmt = sqlsrv_query( $conn, $sql);	
}
?>

<html>
<head>	
</head>

<body>
	<header class="page-header">
		<h2>IVR Daily</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Historical</span></li>
				<li><a href="?ivr=ivrday"><span>IVR Daily</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<form class="form-horizontal" action="?ivr=ivrday" method="post">
		<section class="panel panel-dark">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>

				<h2 class="panel-title">Search</h2>
			</header>
			
			<div class="panel-body">			
					<div class="form-group">
						<label class="col-md-3 control-label">Date</label>
						<div class="col-md-6">
							<div class="input-daterange input-group" data-plugin-datepicker>
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input type="text" class="form-control" name="date1" required/>
								<span class="input-group-addon">to</span>
								<input type="text" class="form-control" name="date2" required/>
							</div>
						</div>
					</div>
			</div>
			
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-default">Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
					</div>
				</div>
			</footer>		
		</section>
	</form>
	
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Result</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped table-condensed mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
				<thead>
					<tr>
					  <th>Tanggal</th>
					  <th>Medan</th>
					  <th>Jakarta</th>
					  <th>Surabaya</th>
					  <th>Makasar</th>
					  <th>Prompter Error</th>
					  <th>MKIOS</th>
					  <th>Long Number</th>
					  <th>International</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
					{
					?>
					<tr class="gradeX">
					  <td><?php echo $r['tgl']; ?></td>
					  <td><?php echo $r['Medan']; ?></td>
					  <td><?php echo $r['Jakarta']; ?></td>
					  <td><?php echo $r['Surabaya']; ?></td>
					  <td><?php echo $r['Makasar']; ?></td>
					  <td><?php echo $r['Prompter_Error']; ?></td>
					  <td><?php echo $r['MKios']; ?></td>
					  <td><?php echo $r['Long_Number']; ?></td>
					  <td><?php echo $r['International']; ?></td>
					</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>

</body>
</html>