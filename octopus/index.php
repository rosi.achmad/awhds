<!doctype html>
<html class="fixed sidebar-left-collapsed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>HDS</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/select2/select2.css" />
		<link rel="stylesheet" href="assets/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<link rel="stylesheet" href="assets/vendor/pnotify/pnotify.custom.css" />
		
		<link rel="stylesheet" href="assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="assets/vendor/morris/morris.css" />
		
		
		<link rel="stylesheet" href="assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="assets/vendor/dropzone/css/basic.css" />
		<link rel="stylesheet" href="assets/vendor/dropzone/css/dropzone.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css" />
		<link rel="stylesheet" href="assets/vendor/summernote/summernote.css" />
		<link rel="stylesheet" href="assets/vendor/summernote/summernote-bs3.css" />
		<link rel="stylesheet" href="assets/vendor/codemirror/lib/codemirror.css" />
		<link rel="stylesheet" href="assets/vendor/codemirror/theme/monokai.css" />
		

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="index.php" class="logo">
						<img src="assets/images/logo-inf.png" height="35" alt="Porto Admin" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
								<span class="name">Administrator</span>
								<span class="role">administrator</span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="#"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							MENU
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li id="home"><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i><span>Home</span></a></li>
									<li id="realtime" class="nav-parent">
										<a>
											<i class="fa fa-refresh	" aria-hidden="true"></i>
											<span>Realtime</span>
										</a>
										<ul class="nav nav-children">
											<li> <a href="?agent=skillreal"><i class="icon icon-refresh"></i> <span>Agent Skill Realtime</span></a> </li>
											<li> <a href="?skill=skillreal"><i class="icon icon-refresh"></i> <span>Skill Group Realtime</span></a> </li>
											<li> <a href="?out=dialerport"><i class="icon icon-refresh"></i> <span>Dialer Port Realtime</span></a> </li>
											<li> <a href="?out=dialerdetail"><i class="icon icon-inbox"></i> <span>Dialer Detail</span></a> </li>											
										</ul>
									</li>
									<li id="historical" class="nav-parent">
										<a>
											<i class="fa fa-copy" aria-hidden="true"></i>
											<span>Historical</span>
										</a>
										<ul class="nav nav-children">
											<li> <a href="?agent=auxdetail"><i class="icon icon-inbox"></i> <span>Agent AUX Detail</span></a></li>
											<li> <a href="?agent=logout"><i class="icon icon-inbox"></i> <span>Agent Login Logout</span></a> </li>
											<li> <a href="?agent=performance"><i class="icon icon-inbox"></i> <span>Agent Performance</span></a></li>
											<li> <a href="?agent=aqh"><i class="icon icon-inbox"></i> <span>Agent Quarter Hour</span></a></li>
											<li> <a href="?ivr=ivrint"><i class="icon icon-inbox"></i> <span>IVR Interval</span></a></li>
											<li> <a href="?ivr=ivrday"><i class="icon icon-inbox"></i> <span>IVR Daily</span></a></li>
											<li> <a href="?skill=daily"><i class="icon icon-inbox"></i> <span>Skill Group Daily</span></a> </li>
											<li> <a href="?skill=interval"><i class="icon icon-inbox"></i> <span>Skill Group Interval</span></a></li>											
										</ul>
									</li>
									<li id="chart" class="nav-parent">
										<a>
											<i class="fa fa-bar-chart-o" aria-hidden="true"></i>
											<span>Chart</span>
										</a>
										<ul class="nav nav-children">
											<li> <a href="?chartskill=interval"><i class="icon icon-inbox"></i> <span>Skill Interval</span></a></li>											
										</ul>
									</li>
									<li><a href="?user=admin"><i class="fa fa-table" aria-hidden="true"></i><span>User</span></a></li>
								</ul>
							</nav>
						</div>				
					</div>
				
				</aside>
				
				<!-- end: sidebar -->
				
				<!-- start: page -->
								
				<section role="main" class="content-body">				
				<?php
				if ($_GET["out"] == "dialerport"){
				include ('dialerport.php');
				}
				else if ($_GET["out"] == "dialerdetail"){
				include ('dialerdetail.php');
				}
				else if ($_GET["agent"] == "skillreal"){
				include ('agentskillrealtime.php');
				}
				else if ($_GET["skill"] == "skillreal"){
				include ('skillgrouprealtime.php');
				}
				else if ($_GET["agent"] == "aqh"){
				include ('agentqhour.php');
				}
				else if ($_GET["agent"] == "logout"){
				include ('agentlogout.php');
				}
				else if ($_GET["agent"] == "auxdetail"){
				include ('auxdetail.php');
				}
				else if ($_GET["skill"] == "interval"){
				include ('skillinterval.php');
				}
				else if ($_GET["skill"] == "daily"){
				include ('skilldaily.php');
				}
				else if ($_GET["agent"] == "performance"){
				include ('agentperformance.php');
				}
				else if ($_GET["ivr"] == "ivrint"){
				include ('ivrinterval.php');
				}
				else if ($_GET["ivr"] == "ivrday"){
				include ('ivrdaily.php');
				}
				else if ($_GET["ccif"] == "user"){
				include ('ccif.php');
				}
				else if ($_GET["user"] == "admin"){
				include ('user.php');
				}
				else if ($_GET["chartskill"] == "interval"){
				include ('chartskillinterval.php');
				}
				else if ($_GET["ccif"] == "add" || $_GET["edit"] || $_GET["delete"] || $_GET["ccif"] == "saveadd" || $_GET["ccif"] == "saveedit"){
				include ('ccif-include.php');
				}
				else {
				include ('home.php');
				}
				?> 					
				</section>
				
				<!-- end: page -->
				
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget">
								<h6>QUERY DATA</h6><br>
								<?php echo $sql; ?>
							</div>
			
						</div>
					</div>
				</div>
			</aside>
		</section>
		
		<!-- Vendor -->
		<script src="assets/vendor/jquery/jquery.js"></script>
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="assets/vendor/select2/select2.js"></script>
		<script src="assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		<script src="assets/vendor/jquery-validation/jquery.validate.js"></script>		
		<script src="assets/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="assets/vendor/flot/jquery.flot.js"></script>
		<script src="assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="assets/vendor/flot/jquery.flot.pie.js"></script>
		<script src="assets/vendor/flot/jquery.flot.categories.js"></script>
		<script src="assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="assets/vendor/raphael/raphael.js"></script>
		<script src="assets/vendor/morris/morris.js"></script>
		<script src="assets/vendor/gauge/gauge.js"></script>
		<script src="assets/vendor/snap-svg/snap.svg.js"></script>
		<script src="assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="assets/vendor/pnotify/pnotify.custom.js"></script>
		
		<script src="assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="assets/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
		<script src="assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
		<script src="assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
		<script src="assets/vendor/fuelux/js/spinner.js"></script>
		<script src="assets/vendor/dropzone/dropzone.js"></script>
		<script src="assets/vendor/bootstrap-markdown/js/markdown.js"></script>
		<script src="assets/vendor/bootstrap-markdown/js/to-markdown.js"></script>
		<script src="assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
		<script src="assets/vendor/codemirror/lib/codemirror.js"></script>
		<script src="assets/vendor/codemirror/addon/selection/active-line.js"></script>
		<script src="assets/vendor/codemirror/addon/edit/matchbrackets.js"></script>
		<script src="assets/vendor/codemirror/mode/javascript/javascript.js"></script>
		<script src="assets/vendor/codemirror/mode/xml/xml.js"></script>
		<script src="assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js"></script>
		<script src="assets/vendor/codemirror/mode/css/css.js"></script>
		<script src="assets/vendor/summernote/summernote.js"></script>
		<script src="assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
		<script src="assets/vendor/ios7-switch/ios7-switch.js"></script>
				
		<!-- Theme Base, Components and Settings -->
		<script src="assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="assets/javascripts/tables/examples.datatables.default.js"></script>
		<script src="assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="assets/javascripts/tables/examples.datatables.tabletools.js"></script>
		<script src="assets/javascripts/forms/examples.validation.js"></script>
		<script src="assets/javascripts/ui-elements/morrisLine.charts.js"></script>
		<script src="assets/javascripts/ui-elements/user.modals.js"></script>
		<script src="assets/javascripts/forms/examples.advanced.form.js" /></script>
		
		<script>
			<?php if ($_GET["agent"] == "auxdetail" || $_GET["agent"] == "logout" || $_GET["agent"] == "performance" || $_GET["agent"] == "aqh" ||$_GET["skill"] == "interval" || $_GET["skill"] == "daily"){ ?>
			   var element = document.getElementById("historical");
			   element.classList.add("nav-active");
			<?php } 
			else if ($_GET["agent"] == "skillreal" || $_GET["out"] == "dialerport" || $_GET["out"] == "dialerdetail") {?>
				var element = document.getElementById("realtime");
				element.classList.add("nav-active");
			<?php } 
			else if ($_GET["chartskill"] == "interval") {?>
				var element = document.getElementById("chart");
				element.classList.add("nav-active");
			<?php } 
			else {?>
				var element = document.getElementById("home");
				element.classList.add("nav-active");
			<?php } ?>
		</script>
	</body>
</html>