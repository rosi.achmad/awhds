<?php
$serverName = "172.28.2.22"; 
$connectionInfo = array( "Database"=>"ucce_awdb", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

$sql = "SELECT b.CampaignName as campaign ,COUNT(a.PhoneNumber) as total
		FROM t_Dialer_Port_Real_Time as a , t_Campaign as b
		where a.CampaignID = b.CampaignID
		and a.PhoneNumber != ''
		GROUP BY b.CampaignName
		ORDER BY b.CampaignName";
$params = array();
$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
$stmt = sqlsrv_query( $conn, $sql, $params, $options);

if( $stmt === false ) {
     die( print_r( sqlsrv_errors(), true));
}
$numrow = sqlsrv_num_rows( $stmt );
		
$sql2 = "SELECT COUNT(*) as con
		FROM t_Dialer_Port_Real_Time 
		where PhoneNumber != ''";
$usage = sqlsrv_query( $conn, $sql2);
?>

<html>
<head>	
	<script>
		function timedRefresh(timeoutPeriod) {
			setTimeout("location.reload(true);",timeoutPeriod);
		}

		window.onload = timedRefresh(3000);
	</script>
</head>

<body>
	<header class="page-header">
		<h2>Dialer Port</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Realtime</span></li>
				<li><a href="?out=dialerport"><span>Dialer Port</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<h5 class="label">Active Campaign = <?php echo $numrow; ?> </h5>
				<h5 class="label">Port Usage = <?php while( $ru = sqlsrv_fetch_array( $usage, SQLSRV_FETCH_ASSOC) ){echo $ru['con'];}?></h5>
			</div>

			<h2 class="panel-title">Result</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered mb-none">
				<thead>
					<tr>
					  <th>Campaign</th>
					  <th>Total Port</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
					{
					?>
					<tr>
					  <td><?php echo $r['campaign']; ?></td>
					  <td><?php echo $r['total']; ?></td>
					</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>

</body>
</html>