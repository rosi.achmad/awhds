<?php
$serverName = "172.28.2.216"; 
$connectionInfo = array( "Database"=>"AWHDS-MIRROR", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

if ($_POST["lagent"]){
	foreach ($_POST["lagent"] as $value){
		$val .= $value;
	}
	$val = rtrim($val,",");
	
	$sql = "select convert(VARCHAR, a.DateTime,103) as DateTime, 
			LEFT(convert(VARCHAR, a.DateTime,108),5) as Time,
			a.EnterpriseName as EnterpriseName, 
			a.PeripheralNumber as PeripheralNumber, 
			a.CallsAnswered as CallsAnswered, a.IncomingCallsOnHold as IncomingCallsOnHold, a.AbandonRingCalls as AbandonRingCalls, 
			CONVERT(VARCHAR, DATEADD(second,a.HandledCallsTime,0),108) as HandledCallsTime, 
			CONVERT(VARCHAR, DATEADD(second,a.IncomingCallsOnHoldTime,0),108) as IncomingCallsOnHoldTime,
			CONVERT(VARCHAR, DATEADD(second,b.AvailTime,0),108) as AvailTime, 
			CONVERT(VARCHAR, DATEADD(second,b.NotReadyTime,0),108) as NotReadyTime, 
			CONVERT(VARCHAR, DATEADD(second,b.LoggedOnTime,0),108) as LoggedOnTime, 
			CONVERT(VARCHAR, DATEADD(second,a.TalkInTime,0),108) as TalkInTime,
			cast( cast(round(((a.TalkInTime + a.TalkOutTime + a.HoldTime + a.WorkReadyTime + a. WorkNotReadyTime + b.NotReadyTime) * 10000 / b.LoggedOnTime * 0.01),2) AS DECIMAL(18,2)) as varchar(100)) + ' %' as Occupancy
			FROM
			(select a.DateTime as DateTime, a.SkillTargetID as SkillTargetID, 
			b.EnterpriseName as EnterpriseName, b.PeripheralNumber as PeripheralNumber,
			sum(a.CallsAnswered) as CallsAnswered, 
			sum(a.IncomingCallsOnHold) as IncomingCallsOnHold, 
			sum(a.AbandonRingCalls) as AbandonRingCalls, 
			sum(a.HandledCallsTime) as HandledCallsTime, 
			sum(a.IncomingCallsOnHoldTime) as IncomingCallsOnHoldTime, 
			sum(a.TalkInTime) as TalkInTime,
			sum(a.TalkOutTime) as TalkOutTime,
			sum(a.HoldTime) as HoldTime,
			sum(a.WorkReadyTime) as WorkReadyTime,
			sum(a.WorkNotReadyTime) as WorkNotReadyTime
			FROM t_Agent_Skill_Group_Interval as a, t_Agent as b
			WHERE a.SkillTargetID = b.SkillTargetID
			and a.DateTime BETWEEN '".$_POST["date1"]." 00:00:00' and '".$_POST["date2"]." 23:45:00'
			GROUP BY a.DateTime, a.SkillTargetID, b.EnterpriseName, b.PeripheralNumber) as a,
			(select a.DateTime as DateTime, a.SkillTargetID as SkillTargetID, 
			b.EnterpriseName as EnterpriseName, b.PeripheralNumber as PeripheralNumber,
			sum(a.AvailTime) as AvailTime, 
			sum(a.NotReadyTime) as NotReadyTime, 
			sum(a.LoggedOnTime) as LoggedOnTime
			FROM t_Agent_Interval as a, t_Agent as b
			WHERE a.SkillTargetID = b.SkillTargetID
			and a.DateTime BETWEEN '".$_POST["date1"]." 00:00:00' and '".$_POST["date2"]." 23:45:00'
			GROUP BY a.DateTime, a.SkillTargetID, b.EnterpriseName, b.PeripheralNumber) as b
			WHERE a.DateTime = b.DateTime
			and a.SkillTargetID = b.SkillTargetID
			and a.PeripheralNumber in (".$val.")
			ORDER BY a.DateTime";			

	$stmt = sqlsrv_query( $conn, $sql);	
}

$sql2 = "SELECT * FROM t_Agent ORDER BY EnterpriseName";
$agent = sqlsrv_query( $conn, $sql2);

?>

<html>
<head>	
</head>

<body>
	<header class="page-header">
		<h2>Agent Quarter Hour</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Historical</span></li>
				<li><a href="?agent=aqh"><span>Agent Quarter Hour</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<form class="form-horizontal" action="?agent=aqh" method="post">
		<section class="panel panel-dark">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>

				<h2 class="panel-title">Search</h2>
			</header>
			
			<div class="panel-body">			
					<div class="form-group">
						<label class="col-md-3 control-label">Date</label>
						<div class="col-md-6">
							<div class="input-daterange input-group" data-plugin-datepicker>
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input type="text" class="form-control" name="date1" required/>
								<span class="input-group-addon">to</span>
								<input type="text" class="form-control" name="date2" required/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Select Agent</label>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-chevron-circle-down"></i>
								</span>
								<select name="lagent[]" multiple data-plugin-selectTwo class="form-control populate" required/>
									<?php	while( $rs = sqlsrv_fetch_array( $agent, SQLSRV_FETCH_ASSOC)) { ?>
									<option value="<?php echo $rs['PeripheralNumber'].","; ?>"><?php echo $rs['EnterpriseName']." [".$rs['PeripheralNumber']."]"; ?></option>
									<?php } ?>						
								</select>
							</div>
						</div>
					</div>	
				
			</div>
			
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-default">Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
					</div>
				</div>
			</footer>		
		</section>
	</form>
	
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Result</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped table-condensed mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
				<thead>
					<tr>
					  <th>Date</th>
					  <th>Time</th>
					  <th>EnterpriseName</th>
					  <th>LoginID</th>
					  <th>ACD</th>
					  <th>Hold</th>
					  <th>ABND</th>
					  <th>ACDTime</th>
					  <th>HoldTime</th>
					  <th>AvailTime</th>
					  <th>NotReadyTime</th>
					  <th>LoggedOnTime</th>
					  <th>TalkInTime</th>
					  <th>Occupancy</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
					{
					?>
					<tr>
					  <td><?php echo $r['DateTime']; ?></td>
					  <td><?php echo $r['Time']; ?></td>
					  <td><?php echo $r['EnterpriseName']; ?></td>
					  <td><?php echo $r['PeripheralNumber']; ?></td>
					  <td><?php echo $r['CallsAnswered']; ?></td>
					  <td><?php echo $r['IncomingCallsOnHold']; ?></td>
					  <td><?php echo $r['AbandonRingCalls']; ?></td>
					  <td><?php echo $r['HandledCallsTime']; ?></td>
					  <td><?php echo $r['IncomingCallsOnHoldTime']; ?></td>
					  <td><?php echo $r['AvailTime']; ?></td>
					  <td><?php echo $r['NotReadyTime']; ?></td>
					  <td><?php echo $r['LoggedOnTime']; ?></td>
					  <td><?php echo $r['TalkInTime']; ?></td>
					  <td><?php echo $r['Occupancy']; ?></td>	
					</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>

</body>
</html>