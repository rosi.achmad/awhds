<?php
$serverName = "172.28.2.22"; 
$connectionInfo = array( "Database"=>"ucce_awdb", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

$sql = "SELECT a.CampaignName as CampaignName, b.CampaignID as CampaignID, b.QueryRuleID as QueryRuleID
		FROM t_Campaign as a, t_Campaign_Query_Rule as b
		WHERE a.CampaignID = b.CampaignID
		ORDER BY a.CampaignName";

$camp = sqlsrv_query( $conn, $sql);

?>

<html>
<head>	
	<?php if ($_POST["campaign"]){ ?>
	<script>
	function timedRefresh(timeoutPeriod) {
		setTimeout("location.reload(true);",timeoutPeriod);
	}

	window.onload = timedRefresh(10000);
	</script>
	<?php } ?>
</head>

<body>
	<header class="page-header">
		<h2>Dialer Detail</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Realtime</span></li>
				<li><a href="?out=dialerdetail"><span>Dialer Detail</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<form class="form-horizontal" action="?out=dialerdetail" method="post">
		<section class="panel panel-dark">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>

				<h2 class="panel-title">Search</h2>
			</header>
			
			<div class="panel-body">
				<div class="form-group">
					<label class="col-md-3 control-label">Select Campaign</label>
					<div class="col-md-6">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-chevron-circle-down"></i>
							</span>
							<select name="campaign" data-plugin-selectTwo class="form-control populate" required/>
								<?php	while( $rc = sqlsrv_fetch_array( $camp, SQLSRV_FETCH_ASSOC)) { ?>
								<option value=<?php echo "DL_".$rc['CampaignID']."_".$rc['QueryRuleID']; ?>><?php echo $rc['CampaignName']; ?></option>
								<?php } ?>						
							</select>
						</div>
					</div>
				</div>				
			</div>
			
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-default">Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
					</div>
				</div>
			</footer>		
		</section>
	</form>
	
	<?php
	if ($_POST['campaign']){
		$serverName = "172.28.2.12"; 
		$connectionInfo = array( "Database"=>"ucce_baA", "UID"=>"sa", "PWD"=>"P@ssw0rd");
		$conn = sqlsrv_connect( $serverName, $connectionInfo);
		
		$result = sqlsrv_query( $conn, "select CallResult, count(*) as total
										from ".$_POST['campaign']." group by CallResult");	
	
	?>
			
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>
				<?php
				$serverName2 = "172.28.2.22"; 
				$connectionInfo2 = array( "Database"=>"ucce_awdb", "UID"=>"sa", "PWD"=>"P@ssw0rd");
				$conn2 = sqlsrv_connect( $serverName2, $connectionInfo2);
				$sql3 = "SELECT * FROM t_Campaign where CampaignID = ".substr($_POST['campaign'], 3, 4)."";
				$campname = sqlsrv_query( $conn2, $sql3);				
				?>
			<h2 class="panel-title"><?php while($rn = sqlsrv_fetch_array( $campname, SQLSRV_FETCH_ASSOC)) { echo $rn['CampaignName']; } ?></h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered mb-none">
				<thead>
					<tr>
					  <th>Result</th>
					  <th>Count</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) 
					{
					?>
					<tr>
					  <td>
					    <?php if($row[CallResult]==0){ echo '(0)Dialer has not yet attempted to contact that customer record';}
						if($row[CallResult]==2){ echo '(2)Error condition while dialing';}		
						if($row[CallResult]==3){ echo '(3)Number reported not in service by network';}	
						if($row[CallResult]==4){ echo '(4)No ringback from network when dial attempted';}		
						if($row[CallResult]==5){ echo '(5)Operator intercept returned from network when dial attempted';}		
						if($row[CallResult]==6){ echo '(6)No dial tone when dialer port went off hook';}		
						if($row[CallResult]==7){ echo '(7)Number reported as invalid by the network';}		
						if($row[CallResult]==8){ echo '(8)Customer phone did not answer';}		
						if($row[CallResult]==9){ echo '(9)Customer phone was busy';}		
						if($row[CallResult]==10){ echo '(10)Customer answered and was connected to agent';}		
						if($row[CallResult]==11){ echo '(11)Fax machine detected';}		
						if($row[CallResult]==12){ echo '(12)Answering machine detected';}		
						if($row[CallResult]==13){ echo '(13)Dialer stopped dialing customer due to lack of agents or network stopped dialing before it was complete';}		
						if($row[CallResult]==14){ echo '(14)Customer requested callback';}		
						if($row[CallResult]==15){ echo '(15)Call was abandoned by the dialer due to lack of agents';}
						if($row[CallResult]==16){ echo '(16)Failed to reserve agent for personal callback';}		
						if($row[CallResult]==17){ echo '(17)Agent has skipped or rejected a preview call';}		
						if($row[CallResult]==18){ echo '(18)Agent has skipped or rejected a preview call with the close option';}		
						if($row[CallResult]==19){ echo '(19)Customer has been abandoned to an IVR';}		
						if($row[CallResult]==20){ echo '(20)Customer dropped call within configured abandoned time';}		
						if($row[CallResult]==21){ echo '(21)Mostly used with TDM switches - network answering machine, such as a network voicemail';}		
						if($row[CallResult]==22){ echo '(22)Number successfully contacted but wrong number';}		
						if($row[CallResult]==23){ echo '(23)Number successfully contacted but reached the wrong person';}		
						if($row[CallResult]==24){ echo '(24)Dialer has flushed this record due to a change in the skillgroup, the campaign, etc.';}		
						if($row[CallResult]==25){ echo '(25)The number was on the do not call list';}		
						if($row[CallResult]==26){ echo '(26)Call disconnected by the carrier or the network while ringing';}		
						if($row[CallResult]==27){ echo '(27)Dead air or low voice volume call';}?>
					  </td>
					  <td><?php echo $row['total']; ?></td>
					</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>
	<?php } ?>
</body>
</html>