<html>
<?php
$conn = mysqli_connect('172.28.2.40', 'appdev', 'appdev!@#', 'ccif')or
    die("Could not connect: " . mysqli_error());

$sql = "SELECT * FROM idm_user limit 100";
$query = mysqli_query($conn, $sql);
?>

<head>	
</head>

<body>
	<header class="page-header">
		<h2>USER</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><a href="?user=admin"><span>User</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>
	
			<h2 class="panel-title">USER</h2>
		</header>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-6">
					<div class="mb-md">
						<a class="mb-xs mt-xs mr-xs modal-with-zoom-anim btn btn-default" href="#addlogin">Add <i class="fa fa-plus"></i></a>
					</div>
				</div>
			</div>
			
			<!-- Modal Form -->
			<div id="addlogin" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
				<form id="form" action="?user=admin" class="form-horizontal">
					<section class="panel">
						<header class="panel-heading">
							<h2 class="panel-title">Add User</h2>
						</header>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label">Name <span class="required">*</span></label>
								<div class="col-sm-9">
									<input type="text" name="name" class="form-control" placeholder="eg.: John Doe" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Password <span class="required">*</span></label>
								<div class="col-sm-9">
									<input type="password" name="password" class="form-control" placeholder="Password" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Email</label>
								<div class="col-sm-9">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
										</span>
										<input type="email" name="email" class="form-control" placeholder="eg.: email@email.com">
									</div>
								</div>
							</div>							
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-sm-9 col-sm-offset-3">
									<button class="btn btn-primary">Submit</button>
									<button type="reset" class="btn btn-default">Reset</button>
									<button class="btn btn-default modal-dismiss">Cancel</button>
								</div>
							</div>
						</footer>
					</section>
				</form>
			</div>
									
			<table class="table table-bordered table-striped mb-none" id="datatable-default">
				<thead>
					<tr>
						<th>Login</th>
						<th>Password</th>
						<th>Name</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Region</th>
						<th>Agent Status</th>
						<th>Person Status</th>
						<th>Agent URN</th>
						<th>Person URN</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while($r = mysqli_fetch_array($query, MYSQLI_ASSOC)){
						$username=$r['username'];
					?>
					<tr>
						<td><?php echo $username; ?></td>
						<td><?php echo $r['password']; ?></td>
						<td><?php echo $r['firstname']; ?></td>
						<td><?php echo $r['startdate']; ?></td>
						<td><?php echo $r['enddate']; ?></td>
						<td><?php echo $r['regionname']; ?></td>
						<td><?php echo $r['agent_provision_status']; ?></td>
						<td><?php echo $r['person_provision_status']; ?></td>
						<td><?php echo $r['agent_urn']; ?></td>
						<td><?php echo $r['person_urn']; ?></td>
						<td class="actions">
							<a href="?edit=<?php echo $username; ?>" class="on-default edit-row"><i class="fa fa-pencil"></i></a>
							<a href="#delete" class="on-default remove-row modal-with-zoom-anim"><i class="fa fa-trash-o"></i></a>
							<div id="delete" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Are you sure?</h2>
									</header>
									<div class="panel-body">
										<div class="modal-wrapper">
											<div class="modal-text">
												<p>Are you sure that you want to delete this row?</p>
											</div>
										</div>
									</div>
									<form>
									<footer class="panel-footer">
										<div class="row">
											<div class="col-md-12 text-right">
												<button id="dialogConfirm" class="btn btn-primary modal-confirm-remove">Confirm</button>
												<button id="dialogCancel" class="btn btn-default modal-dismiss">Cancel</button>
											</div>
										</div>
									</footer>
									</form>
								</section>
							</div>	
						</td>
					</tr>			
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>
	


	
</body>
</html>