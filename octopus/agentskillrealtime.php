<?php
$serverName = "172.28.2.22"; 
$connectionInfo = array( "Database"=>"ucce_awdb", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

if ($_POST["lskill"]){

	$sql = "SELECT c.EnterpriseName as name, a.Extension as ext, d.EnterpriseName as skill, 
			CASE a.AgentState  
					WHEN 0 THEN 'Logged Out' 
					WHEN 1 THEN 'Logged On' 
					WHEN 2 THEN 'Not Ready' 
					WHEN 3 THEN 'Ready' 
					WHEN 4 THEN 'Talking' 
					WHEN 5 THEN 'Work Not Ready' 
					WHEN 6 THEN 'Work Ready' 
					WHEN 7 THEN 'Busy Other' 
					WHEN 8 THEN 'Reserved'  
					WHEN 9 THEN 'Unknown' 
					WHEN 10 THEN 'Hold' 
					WHEN 11 THEN 'Active'  
					WHEN 12 THEN 'Paused' 
					WHEN 13 THEN 'Interrupted' 
					WHEN 14 THEN 'Not Active' 
					ELSE CONVERT(VARCHAR, a.AgentState) END state,
			CASE a.Direction 
					WHEN 1 THEN 'In' 
					WHEN 2 THEN 'Out' 
					WHEN 3 THEN 'Other' 
					WHEN 4 THEN 'Other Out/Direct Preview'
					WHEN 5 THEN 'Outbound Reserve'
					WHEN 6 THEN 'Outbound Preview'
					WHEN 7 THEN 'Outbound Predictive/Progressive'
					ELSE '' END direction,
			a.CustomerPhoneNumber as phonenumber
			FROM t_Agent_Real_Time as a, 
			t_Agent_Skill_Group_Real_Time as b, 
			t_Agent as c,
			t_Skill_Group as d
			where a.SkillTargetID = c.SkillTargetID
			and b.SkillTargetID = c.SkillTargetID
			and b.SkillGroupSkillTargetID = d.SkillTargetID
			and b.SkillGroupSkillTargetID = ".$_POST['lskill']."
			ORDER BY c.EnterpriseName";			

	$stmt = sqlsrv_query( $conn, $sql);	
}

$sql2 = "SELECT * FROM t_Skill_Group ORDER BY EnterpriseName";
$agent = sqlsrv_query( $conn, $sql2);

?>

<html>
<head>	
	<?php if ($_POST["lskill"]){ ?>
	<script>
	function timedRefresh(timeoutPeriod) {
		setTimeout("location.reload(true);",timeoutPeriod);
	}

	window.onload = timedRefresh(10000);
	</script>
	<?php } ?>
</head>

<body>
	<header class="page-header">
		<h2>Agent Realtime</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Realtime</span></li>
				<li><a href="?agent=skillreal"><span>Agent Realtime</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<form class="form-horizontal" action="?agent=skillreal" method="post">
		<section class="panel panel-dark">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>

				<h2 class="panel-title">Search</h2>
			</header>
			
			<div class="panel-body">
				<div class="form-group">
					<label class="col-md-3 control-label">Select Skill</label>
					<div class="col-md-6">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-chevron-circle-down"></i>
							</span>
							<select name="lskill" data-plugin-selectTwo class="form-control populate" required/>
								<?php	while( $rs = sqlsrv_fetch_array( $agent, SQLSRV_FETCH_ASSOC)) { ?>
								<option value="<?php echo $rs['SkillTargetID']; ?>"><?php echo $rs['EnterpriseName']." [".$rs['SkillTargetID']."]"; ?></option>
								<?php } ?>						
							</select>
						</div>
					</div>
				</div>				
			</div>
			
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-default">Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
					</div>
				</div>
			</footer>		
		</section>
	</form>
	
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Result</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered mb-none">
				<thead>
					<tr>
					  <th>Name</th>
					  <th>Extension</th>
					  <th>Skill</th>
					  <th>State</th>
					  <th>Direction</th>
					  <th>Phone Number</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
					{
					?>
					<tr>
					  <td><?php echo $r['name']; ?></td>
					  <td><?php echo $r['ext']; ?></td>
					  <td><?php echo $r['skill']; ?></td>
					  <td class="<?php 	if ($r['state']=='Talking'){echo'success';}
										if ($r['state']=='Not Ready'){echo'danger';}
										if ($r['state']=='Ready'){echo'primary';}
										if ($r['state']=='Hold'){echo'warning';}?>"> <?php echo $r['state']; ?></td>					
					  <td><?php echo $r['direction']; ?></td> 
					  <td><?php echo $r['phonenumber']; ?></td>
					</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>

</body>
</html>