(function( $ ) {
		/*
	Morris: Bar
	*/
	Morris.Bar({
		resize: true,
		element: 'morrisBar',
		data: morrisBarData,
		xkey: 'y',
		ykeys: ['a', 'b'],
		labels: ['COF', 'ACD'],
		hideHover: true,
		barColors: ['#27e514', '#0088cc']
	});
}).apply( this, [ jQuery ]);