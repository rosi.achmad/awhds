<?php
$serverName = "172.28.2.216"; 
$connectionInfo = array( "Database"=>"AWHDS-MIRROR", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

if ($_POST["lskill"]){
	foreach ($_POST["lskill"] as $value){
		$val .= $value;
	}
	$val = rtrim($val,",");
	
	$sql = "select b.EnterpriseName as EnterpriseName,
			LEFT(convert(VARCHAR, a.DateTime,108),5) as starttime, 
			SUM(a.ServiceLevelCalls) AS acceptable,
   			SUM(a.ServiceLevelCallsOffered - a.RouterCallsDequeued) as callsoffered, 
			SUM(a.CallsAnswered) as acdcalls, 
			SUM(a.RouterError) as rce,			 
			CONVERT(VARCHAR, DATEADD(second,SUM(a.HandledCallsTalkTime),0),108) as acdtime, 
			CONVERT(VARCHAR, DATEADD(second,SUM(a.IncomingCallsOnHoldTime),0),108) as holdtime,			
			SUM(a.RouterCallsAbandQ) + sum(a.RouterCallsAbandToAgent) as abncalls,
			sum(a.IncomingCallsOnHold) as held,
			CONVERT(VARCHAR, DATEADD(second,SUM(a.AvgHandledCallsTalkTime),0),108) as aht,
			sum(a.HandledCallsTalkTime)+sum(a.IncomingCallsOnHoldTime)as sumaht
   			FROM t_Skill_Group_Interval as a, t_Skill_Group as b
			where a.SkillTargetID = b.SkillTargetID
			and b.SkillTargetID  in ($val) 
			and a.DateTime between '".$_POST["date1"]." 00:00:00' and '".$_POST["date1"]." 23:45:00' 
			GROUP By b.EnterpriseName,LEFT(convert(VARCHAR, a.DateTime,108),5) 
			ORDER BY b.EnterpriseName,LEFT(convert(VARCHAR, a.DateTime,108),5) ASC";			

	$stmt = sqlsrv_query( $conn, $sql);	
}

$sql2 = "SELECT * FROM t_Skill_Group ORDER BY EnterpriseName";
$agent = sqlsrv_query( $conn, $sql2);

?>

<html>
<head>	
</head>

<body>
	<header class="page-header">
		<h2>Skill Group Interval</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Historical</span></li>
				<li><a href="?skill=interval"><span>Skill Group Interval</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<form class="form-horizontal" action="?skill=interval" method="post">
		<section class="panel panel-dark">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>

				<h2 class="panel-title">Search</h2>
			</header>
			
			<div class="panel-body">			
					<div class="form-group">
						<label class="col-md-3 control-label">Date</label>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input name="date1"type="text" data-plugin-datepicker class="form-control" required/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Select Skill</label>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-chevron-circle-down"></i>
								</span>
								<select name="lskill[]" multiple data-plugin-selectTwo class="form-control populate" required/>
									<?php	while( $rs = sqlsrv_fetch_array( $agent, SQLSRV_FETCH_ASSOC)) { ?>
									<option value="<?php echo $rs['SkillTargetID'].","; ?>"><?php echo $rs['EnterpriseName']." [".$rs['SkillTargetID']."]"; ?></option>
									<?php } ?>						
								</select>
							</div>
						</div>
					</div>	
				
			</div>
			
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-default">Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
					</div>
				</div>
			</footer>		
		</section>
	</form>
	
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Result</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped table-condensed mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
				<thead>
					<tr>
					  <th><span class="icon" title="EnterpriseName">Skill</span></th>
					  <th><span class="icon" title="LEFT(convert(VARCHAR, DateTime,108),5)" class="tip-bottom">Interval</span></th>
					  <th><span class="icon" title="SUM(ServiceLevelCallsOffered-RouterCallsDequeued)" class="tip-bottom">COF</span></th>
					  <th><span class="icon" title="SUM(CallsAnswered)" class="tip-bottom">ACD</span></th>
					  <th><span class="icon" title="SUM(ServiceLevelCalls)" class="tip-bottom">Acceptable</span></th>
					  <th><span class="icon" title="SUM(RouterCallsAbandQ)+sum(RouterCallsAbandToAgent)" class="tip-bottom">ABND</span></th>
					  <th><span class="icon" title="SUM(RouterError)" class="tip-bottom">RCE</span></th>
					  <th><span class="icon" title="CONVERT(VARCHAR, DATEADD(second,SUM(HandledCallsTalkTime),0),108)" class="tip-bottom">ACD Time</span></th>
					  <th><span class="icon" title="CONVERT(VARCHAR, DATEADD(second,SUM(IncomingCallsOnHoldTime),0),108)" class="tip-bottom">Hold Time</span></th>
					  <th><span class="icon" title="sum(IncomingCallsOnHold)" class="tip-bottom">Call Held In</span></th>
					</tr>
				</thead>
				<tbody>
					<?php
					while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
					{
					?>
					<tr class="gradeX">
					  <td><?php echo $r['EnterpriseName']; ?></td>
					  <td><?php echo $r['starttime']; ?></td>
					  <td><?php echo $r['callsoffered']; ?></td>
					  <td><?php echo $r['acdcalls']; ?></td>
					  <td><?php echo $r['acceptable']; ?></td>
					  <td><?php echo $r['abncalls']; ?></td>
					  <td><?php echo $r['rce']; ?></td>
					  <td><?php echo $r['acdtime']; ?></td>
					  <td><?php echo $r['holdtime']; ?></td>
					  <td><?php echo $r['held']; ?></td>
					</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>

</body>
</html>