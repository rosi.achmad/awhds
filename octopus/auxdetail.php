<?php
$serverName = "172.28.2.216"; 
$connectionInfo = array( "Database"=>"AWHDS-MIRROR", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

if ($_POST["lagent"]){
	foreach ($_POST["lagent"] as $value){
		$val .= $value;
	}
	$val = rtrim($val,",");
	
	$sql = "SELECT convert(varchar,a.DateTime,120) as DateTime, 
			b.EnterpriseName, b.PeripheralNumber,
			CASE a.Event
			WHEN '1' THEN 'LOGIN'
			WHEN '2' THEN 'LOGOUT'
			WHEN '3' THEN 'NOT READY'
			ELSE 'UNKNOWN' END as Event, 
			c.ReasonText,
			CONVERT(VARCHAR, DATEADD(second,a.Duration,0),108) as Duration
			FROM t_Agent_Event_Detail as a, t_Agent as b, t_Reason_Code as c
			where a.SkillTargetID = b.SkillTargetID
			and a.ReasonCode = c.ReasonCode
			and a.DateTime between '".$_POST["date1"]." 10:00:00' and '".$_POST["date2"]." 11:00:00'
			and b.SkillTargetID in ($val)
			ORDER BY a.DateTime ASC";			

	$stmt = sqlsrv_query( $conn, $sql);	
}

$sql2 = "SELECT * FROM t_Agent ORDER BY EnterpriseName";
$agent = sqlsrv_query( $conn, $sql2);

?>

<html>
<head>	
</head>

<body>
	<header class="page-header">
		<h2>Agent AUX Detail</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Historical</span></li>
				<li><a href="?agent=auxdetail"><span>Agent AUX Detail</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<form class="form-horizontal" action="?agent=auxdetail" method="post">
		<section class="panel panel-dark">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>

				<h2 class="panel-title">Search</h2>
			</header>
			
			<div class="panel-body">			
					<div class="form-group">
						<label class="col-md-3 control-label">Date</label>
						<div class="col-md-6">
							<div class="input-daterange input-group" data-plugin-datepicker>
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input type="text" class="form-control" name="date1" required/>
								<span class="input-group-addon">to</span>
								<input type="text" class="form-control" name="date2" required/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Select Agent</label>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-chevron-circle-down"></i>
								</span>
								<select name="lagent[]" multiple data-plugin-selectTwo class="form-control populate" required/>
									<?php	while( $rs = sqlsrv_fetch_array( $agent, SQLSRV_FETCH_ASSOC)) { ?>
									<option value="<?php echo $rs['SkillTargetID'].","; ?>"><?php echo $rs['EnterpriseName']." [".$rs['PeripheralNumber']."]"; ?></option>
									<?php } ?>						
								</select>
							</div>
						</div>
					</div>	
				
			</div>
			
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-default">Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
					</div>
				</div>
			</footer>		
		</section>
	</form>
	
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Result</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped table-condensed mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
				<thead>
					<tr>
					    <th>DateTime</th>
						<th>Name</th>
						<th>LoginID</th>
						<th>Event</th>
						<th>Reason</th>
						<th>Duration</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
					{
					?>
					<tr>
						<td><?php echo $r['DateTime']; ?></td>
						<td><?php echo $r['EnterpriseName']; ?></td>
						<td><?php echo $r['PeripheralNumber']; ?></td>
						<td><?php echo $r['Event']; ?></td>
						<td><?php echo $r['ReasonText']; ?></td>
						<td><?php echo $r['Duration']; ?></td>					
					</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>

</body>
</html>