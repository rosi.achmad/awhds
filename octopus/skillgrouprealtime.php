<?php
$serverName = "172.28.2.22"; 
$connectionInfo = array( "Database"=>"ucce_awdb", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

if ($_POST["lskill"]){
	
	foreach ($_POST["lskill"] as $value){
	$val .= $value;
	}
	
	$val = rtrim($val,",");
	$sql = "SELECT b.EnterpriseName,a.LoggedOn,a.Avail,a.NotReady,a.TalkingIn,a.Hold,a.RouterCallsQNow
			FROM t_Skill_Group_Real_Time as a, t_Skill_Group as b
			where a.SkillTargetID = b.SkillTargetID
			and a.SkillTargetID in ($val)";
			
	$stmt = sqlsrv_query( $conn, $sql);
}

$sql2 = "SELECT * FROM t_Skill_Group ORDER BY EnterpriseName";
$skill = sqlsrv_query( $conn, $sql2);

?>

<html>
<head>	
	<?php if ($_POST["lskill"]){ ?>
	<script>
	function timedRefresh(timeoutPeriod) {
		setTimeout("location.reload(true);",timeoutPeriod);
	}

	window.onload = timedRefresh(10000);
	</script>
	<?php } ?>
</head>

<body>
	<header class="page-header">
		<h2>Skill Group Realtime</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Realtime</span></li>
				<li><a href="?skill=skillreal"><span>Skill Group Realtime</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<form class="form-horizontal" action="?skill=skillreal" method="post">
		<section class="panel panel-dark">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>

				<h2 class="panel-title">Search</h2>
			</header>
			
			<div class="panel-body">
				<div class="form-group">
					<label class="col-md-3 control-label">Select Skill</label>
					<div class="col-md-6">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-chevron-circle-down"></i>
							</span>
							<select name="lskill[]" multiple data-plugin-selectTwo class="form-control populate" required/>
								<?php	while( $rs = sqlsrv_fetch_array( $skill, SQLSRV_FETCH_ASSOC)) { ?>
								<option value="<?php echo $rs['SkillTargetID'].","; ?>"><?php echo $rs['EnterpriseName']." [".$rs['SkillTargetID']."]"; ?></option>
								<?php } ?>						
							</select>
						</div>
					</div>
				</div>				
			</div>
			
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-default">Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
					</div>
				</div>
			</footer>		
		</section>
	</form>
	
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Result</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped table-condensed mb-none" >
				<thead>
					<tr>
					  <th>Skill</th>
					  <th>LoggedOn</th>
					  <th>Avail</th>
					  <th>NotReady</th>
					  <th>TalkingIn</th>
					  <th>Hold</th>
					  <th>Queue</th>
					</tr>
				</thead>
					<?php
					while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
					{
					?>
				<tbody>
					<tr>
					  <td><?php echo $r['EnterpriseName']; ?></td>
					  <td><?php echo $r['LoggedOn']; ?></td>
					  <td class="<?php 	if ($r['Avail']=='0'){echo'danger';}?>"> <?php echo $r['Avail']; ?></td>
					  <td><?php echo $r['NotReady']; ?></td>
					  <td><?php echo $r['TalkingIn']; ?></td>
					  <td><?php echo $r['Hold']; ?></td>
					  <td class="<?php 	if ($r['RouterCallsQNow']!='0'){echo'warning';}?>"> <?php echo $r['RouterCallsQNow']; ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>

</body>
</html>