<?php
$serverName = "172.28.2.216"; 
$connectionInfo = array( "Database"=>"AWHDS-MIRROR", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

if ($_POST["lagent"]){
	foreach ($_POST["lagent"] as $value){
		$val .= $value;
	}
	$val = rtrim($val,",");
	
	$sql = "SELECT A.*, B.*
			FROM
			(SELECT 
			AID = AI.SkillTargetID,
			dtime = convert(VARCHAR, AI.DateTime,103),
			agent = AG.EnterpriseName,
			agentid = AG.PeripheralNumber,
			LogOnTime = CONVERT(VARCHAR, DATEADD(second,SUM(AI.LoggedOnTime),0),108),
			NotReadyTime = CONVERT(VARCHAR, DATEADD(second,SUM(AI.NotReadyTime),0),108),
			AvailTime = CONVERT(VARCHAR, DATEADD(second,SUM(AI.AvailTime),0),108)
			FROM
			t_Agent_Interval as AI, 
			t_Agent as AG
			WHERE 
			AI.SkillTargetID = AG.SkillTargetID
			AND AI.DateTime BETWEEN '$_POST[date1] 00:00:00:000' AND '$_POST[date2] 23:59:59:999'
			AND AI.SkillTargetID in ($val)
			GROUP BY
			convert(VARCHAR, AI.DateTime,103),
			AG.EnterpriseName,
			AG.PeripheralNumber,
			AI.SkillTargetID) as A,

			(SELECT 
			dt = convert(VARCHAR, AGS.DateTime,103),
			BID = AGS.SkillTargetID,
			ReserveCallsTime = CONVERT(VARCHAR, DATEADD(second,SUM(AGS.ReserveCallsTime),0),108),
			TalkTimeTotalState = CONVERT(VARCHAR, DATEADD(second,SUM(AGS.TalkInTime + AGS.TalkOutTime + AGS.TalkOtherTime),0),108),
			AutoOutCallsTime = CONVERT(VARCHAR, DATEADD(second,SUM(AGS.AutoOutCallsTime),0),108),
			HoldTimeTotal = CONVERT(VARCHAR, DATEADD(second,SUM(AGS.IncomingCallsOnHoldTime + AGS.AgentOutCallsOnHoldTime),0),108),
			TalkTimeTotal = CONVERT(VARCHAR, DATEADD(second,SUM(AGS.HandledCallsTalkTime + AGS.AgentOutCallsTalkTime),0),108),
			ReserveCalls = SUM(AGS.ReserveCalls),
			CallsHeldTotal = SUM(AGS.IncomingCallsOnHold + AGS.AgentOutCallsOnHold),
			CallsHandledTotal = SUM(AGS.CallsHandled + AGS.AgentOutCalls),
			CallsAnswered = SUM(AGS.CallsAnswered),
			AutoOutCalls = SUM(AGS.AutoOutCalls),
			PreviewCalls = SUM(AGS.PreviewCalls),
			Aht = CONVERT(VARCHAR, DATEADD(second,(SUM(AGS.HandledCallsTalkTime + AGS.AgentOutCallsTalkTime) + SUM(AGS.IncomingCallsOnHoldTime + AGS.AgentOutCallsOnHoldTime)) / SUM(AGS.CallsAnswered),0),108)
			FROM t_Agent_Skill_Group_Interval as AGS,
			t_Agent as AGT
			WHERE AGS.SkillTargetID = AGT.SkillTargetID
			AND AGS.DateTime BETWEEN '$_POST[date1] 00:00:00.000' AND '$_POST[date2] 23:59:59.999'
			AND AGS.SkillTargetID in ($val)
			GROUP BY
			convert(VARCHAR, AGS.DateTime,103),
			AGS.SkillTargetID) as B
			WHERE A.dtime = B.dt
			AND A.AID = B.BID";			

	$stmt = sqlsrv_query( $conn, $sql);	
}

$sql2 = "SELECT * FROM t_Agent ORDER BY EnterpriseName";
$agent = sqlsrv_query( $conn, $sql2);

?>

<html>
<head>	
</head>

<body>
	<header class="page-header">
		<h2>Agent Performance</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Historical</span></li>
				<li><a href="?agent=performance"><span>Agent Performance</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<form class="form-horizontal" action="?agent=performance" method="post">
		<section class="panel panel-dark">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>

				<h2 class="panel-title">Search</h2>
			</header>
			
			<div class="panel-body">			
					<div class="form-group">
						<label class="col-md-3 control-label">Date</label>
						<div class="col-md-6">
							<div class="input-daterange input-group" data-plugin-datepicker>
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input type="text" class="form-control" name="date1" required/>
								<span class="input-group-addon">to</span>
								<input type="text" class="form-control" name="date2" required/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Select Agent</label>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-chevron-circle-down"></i>
								</span>
								<select name="lagent[]" multiple data-plugin-selectTwo class="form-control populate" required/>
									<?php	while( $rs = sqlsrv_fetch_array( $agent, SQLSRV_FETCH_ASSOC)) { ?>
									<option value="<?php echo $rs['SkillTargetID'].","; ?>"><?php echo $rs['EnterpriseName']." [".$rs['PeripheralNumber']."]"; ?></option>
									<?php } ?>						
								</select>
							</div>
						</div>
					</div>	
				
			</div>
			
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-default">Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
					</div>
				</div>
			</footer>		
		</section>
	</form>
	
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Result</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped table-condensed mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
				<thead>
					<tr>
					    <th>Date</th>
						<th>Name</th>
						<th>LoginID</th>
						<th>LogOnTime</th>
						<th>NotReadyTime</th>
						<th>AvailTime</th>
						<th>TalkTime</th>
						<th>HoldTime</th>
						<th>TalkTime</th>
						<th>Held</th>
						<th>CallsHandled</th>
						<th>CallsAnswered</th>
						<th>AHT</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
					{
					?>
					<tr>
						<td><?php echo $r['dtime']; ?></td>
						<td><?php echo $r['agent']; ?></td>
						<td><?php echo $r['agentid']; ?></td>
						<td><?php echo $r['LogOnTime']; ?></td>
						<td><?php echo $r['NotReadyTime']; ?></td>
						<td><?php echo $r['AvailTime']; ?></td>
						<td><?php echo $r['TalkTimeTotalState']; ?></td>
						<td><?php echo $r['HoldTimeTotal']; ?></td>
						<td><?php echo $r['TalkTimeTotal']; ?></td>
						<td><?php echo $r['CallsHeldTotal']; ?></td>
						<td><?php echo $r['CallsHandledTotal']; ?></td>
						<td><?php echo $r['CallsAnswered']; ?></td>
						<td><?php echo $r['Aht']; ?></td>	
					</tr>	
					<?php } ?>
				</tbody>
			</table>
		</div>
	</section>

</body>
</html>