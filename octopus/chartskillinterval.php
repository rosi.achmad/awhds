<?php
$serverName = "172.28.2.216"; 
$connectionInfo = array( "Database"=>"AWHDS-MIRROR", "UID"=>"sa", "PWD"=>"P@ssw0rd");
$conn = sqlsrv_connect( $serverName, $connectionInfo);

if( $conn === false) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
}

if ($_POST["lskill"]){
	foreach ($_POST["lskill"] as $value){
		$val .= $value;
	}
	$val = rtrim($val,",");
	
	$sql = "select LEFT(convert(VARCHAR, DateTime,108),5) as starttime, 
			SUM(ServiceLevelCalls) AS acceptable,
   			SUM(ServiceLevelCallsOffered - RouterCallsDequeued) as callsoffered, 
			SUM(CallsAnswered) as acdcalls, 
			SUM(RouterError) as rce,			 
			CONVERT(VARCHAR, DATEADD(second,SUM(HandledCallsTalkTime),0),108) as acdtime, 
			CONVERT(VARCHAR, DATEADD(second,SUM(IncomingCallsOnHoldTime),0),108) as holdtime,			
			SUM(RouterCallsAbandQ) + sum(RouterCallsAbandToAgent) as abncalls,
			sum(IncomingCallsOnHold) as held,
			CONVERT(VARCHAR, DATEADD(second,SUM(AvgHandledCallsTalkTime),0),108) as aht,
			sum(HandledCallsTalkTime)+sum(IncomingCallsOnHoldTime)as sumaht
   			FROM t_Skill_Group_Interval where SkillTargetID  in ($val) 
			and DateTime between '".$_POST["date1"]." 00:00:00' and '".$_POST["date1"]." 23:45:00' 
			GROUP By LEFT(convert(VARCHAR, DateTime,108),5) 
			ORDER BY LEFT(convert(VARCHAR, DateTime,108),5) ASC";			

	$stmt = sqlsrv_query( $conn, $sql);	
}

$sql2 = "SELECT * FROM t_Skill_Group ORDER BY EnterpriseName";
$agent = sqlsrv_query( $conn, $sql2);

?>

<html>
<head>	
</head>

<body>
	<header class="page-header">
		<h2>Skill Group Interval</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Chart</span></li>
				<li><a href="?chartskill=interval"><span>Skill Group Interval</span></a></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	
	<form class="form-horizontal" action="?chartskill=interval" method="post">
		<section class="panel panel-dark">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>

				<h2 class="panel-title">Search</h2>
			</header>
			
			<div class="panel-body">			
					<div class="form-group">
						<label class="col-md-3 control-label">Date</label>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input name="date1"type="text" data-plugin-datepicker class="form-control">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Select Skill</label>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-chevron-circle-down"></i>
								</span>
								<select name="lskill[]" multiple data-plugin-selectTwo class="form-control populate">
									<?php	while( $rs = sqlsrv_fetch_array( $agent, SQLSRV_FETCH_ASSOC)) { ?>
									<option value="<?php echo $rs['SkillTargetID'].","; ?>"><?php echo $rs['EnterpriseName']." [".$rs['SkillTargetID']."]"; ?></option>
									<?php } ?>						
								</select>
							</div>
						</div>
					</div>	
				
			</div>
			
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-9 col-sm-offset-3">
						<button class="btn btn-default">Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
					</div>
				</div>
			</footer>		
		</section>
	</form>
	
	<section class="panel panel-dark">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="fa fa-caret-down"></a>
				<a href="#" class="fa fa-times"></a>
			</div>

			<h2 class="panel-title">Result</h2>
		</header>
		<div class="panel-body">

			<!-- Morris: Bar -->
			<div class="chart chart-md" id="morrisBar"></div>
			<script type="text/javascript">

				var morrisBarData = [<?php while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) )  { ?>
					{
					y: '<?php echo $r['starttime']; ?>',
					a: <?php echo $r['callsoffered']; ?>,
					b: <?php echo $r['acdcalls']; ?> 
					},
					<?php } ?>];

				// See: assets/javascripts/ui-elements/examples.charts.js for more settings.

			</script>

		</div>
	</section>
</body>
</html>